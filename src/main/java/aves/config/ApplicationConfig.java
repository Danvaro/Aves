package aves.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

// TODO: Allow load all settings from property file
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ApplicationConfig {
    
    public static final String PROTOCOL_PREFIX = "123";

    // Handler executor settings
    public static final int MINIMUM_HANDLER_EXECUTOR_THREAD_COUNT = 50;
    public static final int MESSAGE_QUEUE_SIZE_TRESHOLD = 50;
    public static final int MAXIMUM_HANDLER_EXECUTOR_THREAD_COUNT = Runtime.getRuntime().availableProcessors()
                                                                    * ApplicationConfig.MINIMUM_HANDLER_EXECUTOR_THREAD_COUNT;
}
