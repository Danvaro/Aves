package aves.network.system;

import java.net.DatagramPacket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class MessageQueues {
    
    private MessageQueues(){}

    // TODO: in the future split this up into more than 1 queue. so reads and writes are distributed
    public static final BlockingQueue<DatagramPacket> incomingQueue = new ArrayBlockingQueue<>(100000);
    public static final BlockingQueue<DatagramPacket> outgoingQueue = new ArrayBlockingQueue<>(20000);
}
