package aves.network.system;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.DatagramPacket;

@Slf4j
@SuppressWarnings("unused")
public class Sender implements Runnable {
    
    private static boolean running = true;

    @Override
    public void run() {
    	log.trace("Message sender started");

        while(Sender.running) {
            DatagramPacket outgoingPacket = null;
            
            try {
                outgoingPacket = MessageQueues.outgoingQueue.take();
            } catch (InterruptedException e) {
            	log.error("Sender was stopped");
            	Thread.currentThread().interrupt();
                System.exit(1);
            }
	        
            try {
            	if(outgoingPacket != null) {
		            Socket.getDatagramSocket().send(outgoingPacket);
	            }
            } catch (IOException e) {
                log.error("Udp socket encountered an IO exception!");
            }
        }
    }
    
    public static void stopMessageSender() {
    	Sender.running = false;
    }
	
	public static void startMessageSender() {
		Sender.running = true;
	}

    public static void startSenderThread() {
        (new Thread(new Sender())).start();
    }
}