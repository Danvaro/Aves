package aves.network.system;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.DatagramPacket;

@Slf4j
@SuppressWarnings("unused")
public class Receiver implements Runnable {
    
    private static boolean running = true;

    @Override
    public void run() {
    	log.trace("Message receiver started");

        while (running) {
            byte[] buffer = new byte[100000];
            DatagramPacket datagramPacket = new DatagramPacket(buffer, 0, buffer.length);

            try {
            	if(Socket.getDatagramSocket() != null) {

		            Socket.getDatagramSocket().receive(datagramPacket);
	            }
            } catch (IOException e) {
            	log.error("Udp socket encountered an IO exception!");
            	System.exit(1);
            }

            try {
                MessageQueues.incomingQueue.put(datagramPacket);
            } catch (InterruptedException e) {
            	log.error("Receiver was stopped");
	            Thread.currentThread().interrupt();
	            System.exit(1);
            }
        }
    }
	
	public static void stopMessageReceiver() {
		Receiver.running = false;
	}
	
	public static void startMessageReceiver() {
		Receiver.running = true;
	}

    public static void startReceiverThread() {
        (new Thread(new Receiver())).start();
    }
}
