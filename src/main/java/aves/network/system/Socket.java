package aves.network.system;

import lombok.extern.slf4j.Slf4j;

import java.net.DatagramSocket;
import java.net.SocketException;

@Slf4j
public class Socket {

    private static DatagramSocket datagramSocket;

    public static void initDatagramSocket(int port) {

        try {
            log.trace("Binding socket to port " + port);
            Socket.datagramSocket = new DatagramSocket(port);
        } catch (SocketException e) {
            log.error("There was an error binding on port: " + port + ". port already in use?");
            System.exit(1);
        }
    }
    
    static DatagramSocket getDatagramSocket() {
        return Socket.datagramSocket;
    }
}
