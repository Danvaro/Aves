package aves.network;

import aves.network.system.Receiver;
import aves.network.system.Sender;
import aves.network.system.Socket;

public class AvesNetwork {
    
    private AvesNetwork(){}

    public static void startNetworkStack(int port) {
        Socket.initDatagramSocket(port);

        Sender.startSenderThread();
        Receiver.startReceiverThread();
    }
}
