package aves.state.client;

import aves.entities.User;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@Slf4j
public class SessionManager {

	private static SessionManager instance = null;

	private SessionManager() {
        runTimeOutMonitor();
    }

	public static SessionManager getInstance() {
		if(instance == null) {
			instance = new SessionManager();
		}

		return instance;
	}

	private ConcurrentHashMap<String, User> connectedClientsByIP = new ConcurrentHashMap<>();

	private ConcurrentHashMap<String, String> connectedClientsByUsername = new ConcurrentHashMap<>();
	
	public boolean isClientConnected(User user) {
		return connectedClientsByIP.containsKey(user.getConnectionKey());
	}

	public boolean isClientConnected(String connectionKey) {
		return connectedClientsByIP.containsKey(connectionKey);
	}
	
	public void connectClient(User user) {
		connectedClientsByIP.put(user.getConnectionKey(), user);
	}

	public User getUserByIP(String connectionKey) {
		return connectedClientsByIP.get(connectionKey);
	}

	public void removeSession(String connectionKey) {
	    connectedClientsByIP.remove(connectionKey);
    }

    private void runTimeOutMonitor() {
        Timer t = new Timer();

        log.trace("Timeout Manager started");
        t.schedule(new TimerTask() {

            @Override
            public void run() {
                Date now = new Date();

                for (User user : connectedClientsByIP.values()) {
                    if (TimeUnit.MILLISECONDS.toMillis(now.getTime() - user.getLastMessageReceived().getTime()) > 5000) {
                        if (TimeUnit.MILLISECONDS.toMillis(now.getTime() - user.getLastMessageReceived().getTime()) > 30000) {
                            removeSession(user.getConnectionKey());
                            log.debug("User with connection key " + user.getConnectionKey() + " timed out.");
                        }
                    }
                }

            }
        }, 0, 2000);
    }
}