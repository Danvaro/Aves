package aves.interfaces;

import aves.entities.AvesObject;
import aves.entities.User;

public interface IAvesRequestHandler {
    void handleRequest(User user, AvesObject avesObject);
}
