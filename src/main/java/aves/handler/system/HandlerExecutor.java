package aves.handler.system;

import aves.entities.AvesObject;
import aves.entities.AvesRequest;
import aves.entities.User;
import aves.handler.system.threading.HandlerExecutorThreadPoolManager;
import aves.network.system.MessageQueues;
import lombok.extern.slf4j.Slf4j;

import java.net.DatagramPacket;

@Slf4j
public class HandlerExecutor implements Runnable {

	private boolean running = true;
	private int handlerExecuterId;

	@Override
	public void run() {
		handlerExecuterId = HandlerExecutorThreadPoolManager.handlerExecutorThreadAmount.getAndIncrement();
		log.trace("Handler executor #" + handlerExecuterId + " starting");

		while (running) {
			DatagramPacket incomingPacket = getIncomingDatagramPacketFromQueue();
			
			if (incomingPacket == null) {
				running = false;
				return;
			}

			AvesRequest avesRequest =  MessageValidator.validateMessage(incomingPacket);

			avesRequest.getUser().updateLastMessageReceived();

			if(avesRequest != null) {
                passMessageToHandler(avesRequest.getHandlerId(), avesRequest.getUser(), avesRequest.getPayload());
				log.trace("Handler executor #" + handlerExecuterId + " processed message");
            }
		}
	}

	private DatagramPacket getIncomingDatagramPacketFromQueue() {
		try {
			return MessageQueues.incomingQueue.take();
		} catch (InterruptedException e) {
			log.trace("Handler executor #" + handlerExecuterId + " was shut down by threadpool manager");
			Thread.currentThread().interrupt();
			return null;
		}
	}

	private void passMessageToHandler(String requestId, User user, AvesObject avesObject) {
		if(HandlerManager.getInstance().handlerPresent(requestId)) {
			HandlerManager.getInstance().getHandler(requestId).handleRequest(user, avesObject);
		} else {
			log.error("Handler with request id " + requestId + " does not exist!");
		}
	}
}
