package aves.handler.system.threading;

import java.util.LinkedList;

public class HandlerExecutorThreadPool {

    public static HandlerExecutorThreadPool handlerExecutorThreadPool;

    private LinkedList<Thread> pool;
    private int threadPoolSize = Integer.MAX_VALUE;
    private int maxThreadPoolSize;
    private boolean terminated = false;
    private boolean running = false;
    private Class<? extends Runnable> submission;

    public int getThreadId() {
        return pool.size();
    }

    public HandlerExecutorThreadPool(final Class<? extends Runnable> task) {
        pool = new LinkedList<>();
        submission = task;
    }

    public HandlerExecutorThreadPool(final int poolSize, final Class<? extends Runnable> task) {
        threadPoolSize = poolSize;
        pool = new LinkedList<>();
        submission = task;
    }

    public HandlerExecutorThreadPool(final int poolSize, final int maxPoolSize,
                    final Class<? extends Runnable> task) {
        threadPoolSize = poolSize;
        maxThreadPoolSize = maxPoolSize;
        pool = new LinkedList<>();
        submission = task;
    }

    public final void setPoolSize(final int size)
            throws IllegalArgumentException {
        if (size > maxThreadPoolSize) {
            throw new IllegalArgumentException(
                    "Thread pool size greater than max thread pool size");
        }

        // Only perform actual resizing if the pool is currently running.
        if (running) {
            if (size > threadPoolSize) {
                for (int i = threadPoolSize; i < size; i++) {
                    Thread t;
                    try {
                        t = new Thread(submission.newInstance());
                        t.start();
                        pool.add(t);
                    } catch (InstantiationException | IllegalAccessException e) {
                        shutdown();
                        e.printStackTrace();
                    }
                }
            }
            else if (size < threadPoolSize) {
                LinkedList<Thread> kill = new LinkedList<Thread>();
                // Interrupt all the threads at once until the threadPoolSize is equal to the new size
                for (int i=threadPoolSize; i>size; i--) {
                    Thread t = pool.remove();
                    t.interrupt();
                    kill.add(t);
                }

                // Kill the threads and wait for them to join
                for (Thread t : kill) {
                    try {
                        t.join();
                    } catch (InterruptedException e) {
                        // Interrupted, break loop
                        break;
                    }
                }
            }
        }
        // After resizing is done (or not if the pool isn't running) set the threadPoolSize accordingly
        threadPoolSize = size;
    }

    public final int getPoolSize() {
        return threadPoolSize;
    }

    public final void setMaxPoolSize(final int maxSize) {
        maxThreadPoolSize = maxSize;
    }

    public final int getMaxPoolSize() {
        return maxThreadPoolSize;
    }

    public final void submit(final Class<? extends Runnable> task) {
        submission = task;
    }

    public final void run() throws IllegalStateException, InstantiationException, IllegalAccessException {
        if (!running) {
            if (null == submission) {
                throw new IllegalStateException("No submission passed");
            }

            for (int i=pool.size(); i< threadPoolSize &&
                    i<= maxThreadPoolSize; i++) {
                Thread t;
                try {
                    t = new Thread(submission.newInstance());
                    t.start();
                    pool.add(t);
                }
                catch(InstantiationException | IllegalAccessException e) {
                    shutdown();
                    throw e;
                }
            }

            running = true;
            terminated = false;
        }
    }

    public final void shutdown() {
        while (!pool.isEmpty()) {
            Thread t = pool.remove();
            t.interrupt();
        }
        terminated = true;
        running = false;
    }

    public final boolean isTerminated() {
        return terminated;
    }

    public final boolean isRunning() {
        return running;
    }
}
