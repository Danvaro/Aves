package aves.handler.system.threading;

import aves.config.ApplicationConfig;
import aves.handler.system.HandlerExecutor;
import aves.network.system.MessageQueues;
import lombok.extern.slf4j.Slf4j;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class HandlerExecutorThreadPoolManager implements Runnable {
	
	private Timer timer = new Timer();
	
	public static AtomicInteger handlerExecutorThreadAmount = new AtomicInteger();

	public static void startHandlerExecutorWorkers() {
		HandlerExecutorThreadPool.handlerExecutorThreadPool =
				new HandlerExecutorThreadPool(
						ApplicationConfig.MINIMUM_HANDLER_EXECUTOR_THREAD_COUNT,
						ApplicationConfig.MAXIMUM_HANDLER_EXECUTOR_THREAD_COUNT,
						HandlerExecutor.class);
		try {
			HandlerExecutorThreadPool.handlerExecutorThreadPool.run();
		} catch (InstantiationException e) {
			log.error("Could not instantiate Handler executor thread pool");
		} catch (IllegalAccessException e) {
			log.error("Could not access Handler executor thread pool class");
		}
	}
	
	@Override
	public void run() {
		
		long timerInterval = 1200;
		
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				
				if (MessageQueues.incomingQueue.size() > ApplicationConfig.MESSAGE_QUEUE_SIZE_TRESHOLD) {
					increaseHandlerExecutorThreadAmount();
				} else if (MessageQueues.incomingQueue
						           .size() < ApplicationConfig.MESSAGE_QUEUE_SIZE_TRESHOLD && MessageQueues.incomingQueue
								                                                                   .size() == ApplicationConfig.MINIMUM_HANDLER_EXECUTOR_THREAD_COUNT) {
					decreaseHandlerExecutorThreadAmount();
				}
				
			}
		}, 1000, timerInterval);
	}
	
	public void increaseHandlerExecutorThreadAmount() {
		int threadIncreaseAmount = 2;
		if (HandlerExecutorThreadPool.handlerExecutorThreadPool
				    .getPoolSize() + threadIncreaseAmount <= HandlerExecutorThreadPool.handlerExecutorThreadPool
						                                             .getMaxPoolSize()) {
			log.trace("Adding " + threadIncreaseAmount + " handler executor thread(s) due to higher load");
			
			int newThreadAmount = HandlerExecutorThreadPool.handlerExecutorThreadPool
					                      .getPoolSize() + threadIncreaseAmount;
			HandlerExecutorThreadPool.handlerExecutorThreadPool.setPoolSize(newThreadAmount);
		}
	}
	
	public void decreaseHandlerExecutorThreadAmount() {
		int threadDecreaseAmount = 1;
		if (HandlerExecutorThreadPool.handlerExecutorThreadPool
				    .getPoolSize() - threadDecreaseAmount >= ApplicationConfig.MINIMUM_HANDLER_EXECUTOR_THREAD_COUNT) {
			
			log.trace("Stopping " + threadDecreaseAmount + " handler executor thread(s) due to low load");
			
			int newThreadAmount = HandlerExecutorThreadPool.handlerExecutorThreadPool
					                      .getPoolSize() - threadDecreaseAmount;
			HandlerExecutorThreadPool.handlerExecutorThreadPool.setPoolSize(newThreadAmount);
		}
	}
}
