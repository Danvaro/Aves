package aves.handler.system;

import aves.interfaces.IAvesRequestHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

@Slf4j
public class HandlerManager {

    private static HandlerManager handlerManagerInstance = null;

    private HandlerManager() {}

    public static HandlerManager getInstance() {
        if(handlerManagerInstance == null) {
            handlerManagerInstance = new HandlerManager();
        }

        return handlerManagerInstance;
    }

    private HashMap<Object, Class<?>> handlers = new HashMap<>();

    public void addHandler(String requestId, Class<? extends IAvesRequestHandler> clazz) {
            if (!IAvesRequestHandler.class.isAssignableFrom(clazz)) {
                log.error("Handler with request id " + requestId + " does not implement HandlerExtender!");
                return;
            } else if(handlerPresent(requestId)) {
                log.error("Handler with request id " + requestId + " already exists");
                return;
            }

            log.trace("Adding request handler: " + requestId);
            handlers.put(requestId, clazz);
    }

    public void removeHandler(String requestId) {
        if(handlerPresent(requestId)) {
            handlers.remove(requestId);
            log.trace("Handler with request id " + requestId + " removed");
        }
    }

    public boolean handlerPresent(String requestId) {
        return handlers.containsKey(requestId);
    }

    public IAvesRequestHandler getHandler(String requestId) {
        try {
        	if(handlerPresent(requestId)) {
		        return (IAvesRequestHandler) handlers.get(requestId).newInstance();
	        }
        } catch (InstantiationException e) {
        	log.error("Could not instantiate " + requestId + "handler instance!");
        } catch (IllegalAccessException e) {
        	log.error("Could not access " + requestId + "handler class! is the class public?");
        } catch (NullPointerException e) {
        	log.error("Request id " + requestId + "nullpointer! is the handler removed during runtime?");
        }
        
        return null;
    }
}
