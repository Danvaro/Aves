package aves.handler.system;

import aves.interfaces.IAvesRequestHandler;

public abstract class HandlerExtender {

    public static void addRequestHandler(String requestId, Class<? extends IAvesRequestHandler> clazz) {
        HandlerManager.getInstance().addHandler(requestId, clazz);
    }
}
