package aves.handler.system;

import aves.config.ApplicationConfig;
import aves.entities.User;
import aves.state.client.SessionManager;
import aves.entities.AvesObject;
import aves.entities.AvesRequest;
import aves.util.serializers.Json;
import com.google.gson.JsonSyntaxException;
import lombok.extern.slf4j.Slf4j;

import java.net.DatagramPacket;
import java.util.Objects;
import java.util.regex.Pattern;

@Slf4j
public class MessageValidator {

    static AvesRequest validateMessage(DatagramPacket incomingPacket) {
        return messageValid(incomingPacket);
    }

    private static AvesRequest messageValid(DatagramPacket packetToValidate) {
        String message = new String(packetToValidate.getData());
        log.trace("Received message: " + message);
        
        if(protocolValid(message)) {
            log.trace("Message protocol is valid");
            String[] messageArray = new String(packetToValidate.getData()).split(Pattern.quote("|"));

            if(messageArray.length != 5) {
                log.error("Received message does not meet required protocol standards!");
                return null;
            }

            String connectionKey = packetToValidate.getAddress().toString() + packetToValidate.getPort();

            if(!SessionManager.getInstance().isClientConnected(connectionKey)) {
                if(!messageArray[3].equals("99")) {
                    log.debug("Message validation failed: User is not connected.");
                    return null;
                } else {
                    log.trace("Client trying to connect");
                    return parseToAvesRequestModel(new User(packetToValidate.getAddress(), packetToValidate.getPort()), messageArray);
                }
            }

	        return parseToAvesRequestModel(SessionManager.getInstance().getUserByIP(connectionKey), messageArray);
        }
        
        log.error("Message protocol on received message is invalid!");

        return null;
    }

    private static AvesRequest parseToAvesRequestModel(User user, String[] messageArray) {

        try {
            return AvesRequest.builder().user(user)
                    .messageType(Integer.parseInt(messageArray[1]))
                    .messageSequence(Integer.parseInt(messageArray[2]))
                    .handlerId(messageArray[3])
                    .payload(Json.fromJson(messageArray[4].trim(), AvesObject.class)).build();
            
        } catch (NumberFormatException nfe) {
            log.error("Message cant be parsed to request due to invalid number formatting!");
        } catch (JsonSyntaxException e) {
        	log.error("Could not parse payload to AvesObject!");
        }
        
        return null;
    }

    private static boolean protocolValid(String message) {
        return Objects.equals(message.substring(0, 3), ApplicationConfig.PROTOCOL_PREFIX);
    }
}
