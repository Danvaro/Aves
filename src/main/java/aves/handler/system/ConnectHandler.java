package aves.handler.system;

import aves.entities.AvesObject;
import aves.entities.User;
import aves.interfaces.IAvesRequestHandler;
import aves.state.client.SessionManager;

public class ConnectHandler implements IAvesRequestHandler {
	
	@Override
	public void handleRequest(User user, AvesObject avesObject) {
		System.out.println("Session " + user.getConnectionKey() + " is trying to connect");
		
		if(!SessionManager.getInstance().isClientConnected(user)) {
			SessionManager.getInstance().connectClient(user);
			System.out.println("Session "+ user.getConnectionKey() + " connected.");
			System.out.println("last message received at: " + user.getLastMessageReceived());
		} else {
			System.out.println(avesObject.getAllKeys());
			System.out.println("Session is already connected!");
		}
	}
}
