package aves.system;

import aves.handler.system.*;
import aves.network.AvesNetwork;
import aves.handler.system.threading.HandlerExecutorThreadPoolManager;
import lombok.extern.slf4j.Slf4j;

@SuppressWarnings("unused")
@Slf4j
public class AvesBootstrap {
	
	private AvesBootstrap(){}

    // TODO: add start with property file
    public static void launch() {
        startAvesServer(3500);
    }

    public static void launch(int port) {
        startAvesServer(port);
    }

    public static void launch(String[] args) {
        if (args.length == 1) {
            startAvesServer(Integer.parseInt(args[0]));
        } else if (args.length == 0) {
            startAvesServer(3500);
        } else {
        	log.error("Too many startup parameters provided");
        }
    }

    private static void startAvesServer(int port) {
        AvesNetwork.startNetworkStack(port);

        HandlerExecutorThreadPoolManager.startHandlerExecutorWorkers();
        startHandlerExecutorThreadPoolManager();
	
	    addSystemhandlers();
        
        log.info("Aves Server started on port " + port);
    }
    
    private static void addSystemhandlers() {
        HandlerManager.getInstance().addHandler("99", ConnectHandler.class);
    }

    private static void startHandlerExecutorThreadPoolManager() {
        (new Thread(new HandlerExecutorThreadPoolManager())).start();
    }
}
