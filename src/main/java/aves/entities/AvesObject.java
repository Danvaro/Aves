package aves.entities;

import com.google.gson.annotations.SerializedName;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
//TODO: Split HashMap into different kind of object types

@Slf4j
public class AvesObject {

    @SerializedName("a")
    private HashMap<String, Object> avesObject = new HashMap<>();
    
    public void put(String name, Object value) {
    	if(containsKey(name)) {
    		log.warn("Aves Object already contains key " + name + ". Overriding it.");
	    }
    	avesObject.put(name, value);
    }

    public Integer getInt(String name) {
        try {
            Double stuff = (Double) avesObject.get(name);
            return stuff.intValue();
        } catch (ClassCastException e) {
            log.error("Aves Object key " + name + " is not parsable to an int!", e.getCause());
            return null;
        }
    }

    public Float getFloat(String name) {
        try {
            Double stuff = (Double) avesObject.get(name);
            return stuff.floatValue();
        } catch (ClassCastException e) {
            log.error("Aves Object key " + name + " is not parsable to an float!", e.getMessage());
            return null;
        }
    }

    public String getString(String name) {
        try {
            return (String) avesObject.get(name);
        } catch (ClassCastException e) {
            log.error("Aves Object key " + name + " is not parsable to an String!");
            return null;
        }
    }
    
    public double getDouble(String name) {
        try {
            return (double) avesObject.get(name);
        } catch (ClassCastException e) {
            log.error("Aves Object key " + name + " is not parsable to an double!");
            return 0;
        }
    }
    
    public boolean containsKey(String key) {
        return avesObject.containsKey(key);
    }

    public List<String> getAllKeys() {
        List<String> keys = new ArrayList<>();

        keys.addAll(avesObject.keySet());

        return keys;
    }
}
