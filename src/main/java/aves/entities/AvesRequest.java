package aves.entities;

import lombok.*;

@Builder
@Getter
public class AvesRequest {

    User user;

    //Type 1: unreliable, unordered
    //Type 2: reliable, unordered
    //Type 3: unreliable, ordered
    //Type 4: reliable, ordered
    int messageType;

    int messageSequence;

    String handlerId;

    AvesObject payload;
}
