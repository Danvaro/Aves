package aves.entities;

import lombok.Getter;
import lombok.Setter;

import java.net.InetAddress;
import java.util.Date;

@Getter
@Setter
public abstract class Session {

	InetAddress inetAddress;
	int port;

	Date lastMessageReceived;

	public Session(InetAddress inetAddress, int port) {
		this.inetAddress = inetAddress;
		this.port = port;
		this.lastMessageReceived = new Date();
	}

	public String getConnectionKey() {
		return inetAddress.toString() + port;
	}

	public void updateLastMessageReceived() {
		lastMessageReceived.setTime(System.currentTimeMillis());
	}
}
