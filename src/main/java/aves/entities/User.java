package aves.entities;

import lombok.Getter;
import lombok.Setter;

import java.net.InetAddress;

@Getter
@Setter
public class User extends Session {

    private String userName;

    public User(InetAddress inetAddress, int port) {
        super(inetAddress, port);
    }
}
